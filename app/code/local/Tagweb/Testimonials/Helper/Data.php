<?php
/**
 * Created by PhpStorm.
 * User: andremourapassos
 * Date: 7/11/16
 * Time: 12:35
 */ 
class Tagweb_Testimonials_Helper_Data extends Mage_Core_Helper_Abstract {

    public function getRegisterUrl()
    {
        return $this->_getUrl('testimonials/index/save');
    }

    public function getMainUrl()
    {
        return $this->_getUrl('testimonials');
    }
}