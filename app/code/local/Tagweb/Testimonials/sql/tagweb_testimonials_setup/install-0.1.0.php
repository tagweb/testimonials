<?php
/**
 * Created by PhpStorm.
 * User: andremourapassos
 * Date: 7/11/16
 * Time: 12:35
 */ 
/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

/**
 * Create table 'tagweb_testimonials/post'
 */
$table = $installer->getConnection()
    ->newTable($installer->getTable('tagweb_testimonials/post'))
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
    ), 'id')
    ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'customer_id')
    ->addColumn('comment', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'unsigned'  => true,
        'nullable'  => false,
    ), 'comment');

$installer->getConnection()->createTable($table);

$installer->endSetup();