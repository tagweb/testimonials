<?php

class Tagweb_Testimonials_IndexController extends Mage_Core_Controller_Front_Action{

    public function indexAction(){
        $this->loadLayout()->renderLayout();
    }

    public function saveAction(){

        if($postData = $this->getRequest()->getParams()){

            $customerData = Mage::getSingleton('customer/session')->getCustomer();

            $testimonial = Mage::getModel('tagweb_testimonials/post')
                ->setCustomerId($customerData->getId())
                ->setComment($postData['comment']);

            try{
                $testimonial->save();
                Mage::getSingleton('core/session')->addSuccess($this->__('Testimonials save successfully'));
            } catch(Exception $e){
                Mage::log($e->getMessage(), null, 'tagweb_testimonials.log', true);
            }

        } else {
            Mage::log('Fields not filled', null, 'tagweb_testimonials.log', true);
        }
        $this->_redirect('*/*/');

    }
}