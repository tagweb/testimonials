<?php

class Tagweb_Testimonials_Adminhtml_PostController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('tagweb/post');

    }
    public function indexAction()
    {
        $this->_title($this->__('Tagweb'))->_title($this->__('Testimonials'));
        $this->loadLayout();
        $this->_setActiveMenu('tagweb/post');
        $this->_addContent($this->getLayout()->createBlock('tagweb_testimonials/adminhtml_post'));
        $this->renderLayout();
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('tagweb_testimonials/adminhtml_post_grid')->toHtml()
        );
    }

    public function deleteAction(){
        $this->_initAction();
        $id  = $this->getRequest()->getParam('id');

        $post = Mage::getModel('tagweb_testimonials/post');

        if ($id) {
            $post->load($id);

            if (!$post->getId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('Testimonials dont exist'));
                $this->_redirect('*/*/');
                return;
            } else {
                try{
                    $post->delete();
                    Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Post deleted'));

                }catch(Exception $e){
                    Mage::getSingleton('adminhtml/session')->addError($this->__('Problems to save testimonials, see log'));
                    Mage::log($e, null, 'tagweb_testimonials.log', true);
                }
            }
        }
        $this->_redirect('*/*/');
    }

    public function saveAction(){
        $this->_initAction();
        $id  = $this->getRequest()->getParam('id');

        $post = Mage::getModel('tagweb_testimonials/post');

        if ($id) {
            $comment  = $this->getRequest()->getParam('comment');
            $post->load($id);

            if (!$post->getId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('Testimonials dont exist'));
                $this->_redirect('*/*/');
                return;
            } else {
                $post->setComment($comment);
                try{
                    $post->save();
                    Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Post updated'));

                }catch(Exception $e){
                    Mage::getSingleton('adminhtml/session')->addError($this->__('Problems to save testimonials, see log'));
                    Mage::log($e, null, 'tagweb_testimonials.log', true);
                }
            }
        }
        $this->_redirect('*/*/');
    }


    public function editAction()
    {
        $this->_initAction();

        $id  = $this->getRequest()->getParam('id');
        $post = Mage::getModel('tagweb_testimonials/post');

        if ($id) {
            $post->load($id);
            $customer = Mage::getModel('customer/customer')->load($post->getCustomerId());

            if (!$post->getId()) {
                Mage::getSingleton('adminhtml/session')->addError($this->__('This testimonial no longer exists.'));
                $this->_redirect('*/*/');

                return;
            } else {
                Mage::register('tagweb_testimonials', $post);
                $postContent = $this->getLayout()->createBlock('tagweb_testimonials/adminhtml_post')
                    ->setTemplate('tagweb/testimonials/post.phtml')
                    ->setData('id', $id)
                    ->setData('comment', $post->getComment())
                    ->setData('firstname', $customer->getFirstname())
                    ->setData('lastname', $customer->getLastname())
                    ->setData('email', $customer->getEmail());
                $this->_initAction()
                    ->_addContent($postContent)
                    ->renderLayout();
            }
        }


    }

    /**
     * Initialize action
     *
     * Here, we set the breadcrumbs and the active menu
     *
     * @return Mage_Adminhtml_Controller_Action
     */
    protected function _initAction()
    {
        $this->loadLayout()
            ->_title($this->__('Tagweb'))->_title($this->__('Tagweb'));

        return $this;
    }
}