<?php

class Tagweb_Testimonials_Block_Testimonials extends Mage_Core_Block_Template{

    public function userLoggedIn(){
        return Mage::getSingleton('customer/session')->isLoggedIn();
    }

    public function getPostActionUrl(){
        return Mage::helper('tagweb_testimonials')->getRegisterUrl();
    }

    public function getTestimonials(){

        $html = '';

        $posts = Mage::getModel('tagweb_testimonials/post')->getPosts();

        foreach($posts as $post){
            $html .= '<li>';
            $html .= '<strong>'.$post['name'].'</strong>';
            $html .= '<br>'.$post['comment'];
            $html .= '</li>';
        }

        return $html;
    }
}