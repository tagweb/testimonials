<?php

class Tagweb_Testimonials_Block_Adminhtml_Post extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'tagweb_testimonials';
        $this->_controller = 'adminhtml_post';
        $this->_headerText = Mage::helper('tagweb_testimonials')->__('Testimonials');
        parent::__construct();
        $this->_removeButton('add');
    }
}