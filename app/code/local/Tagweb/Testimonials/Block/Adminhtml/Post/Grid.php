<?php
class Tagweb_Testimonials_Block_Adminhtml_Post_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        $this->setDefaultSort('id');
        $this->setId('id');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }

    protected function _getCollectionClass()
    {
        return 'tagweb_testimonials/post_collection';
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('tagweb_testimonials/post')
            ->getCollection();

        $fn = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'firstname');
        $ln = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'lastname');

        $collection->getSelect()
            ->join(array('sce1' => 'customer_entity_varchar'), 'sce1.entity_id=main_table.customer_id', array('firstname' => 'value'))
            ->join(array('sce2' => 'customer_entity_varchar'), 'sce2.entity_id=main_table.customer_id', array('lastname' => 'value'))
            ->join(array('sce3' => 'customer_entity'), 'sce3.entity_id=main_table.customer_id', array('sce3.email AS customer_mail'))
            ->where('sce1.attribute_id=' . $fn->getAttributeId())
            ->where('sce2.attribute_id=' . $ln->getAttributeId())
            ->columns(new Zend_Db_Expr("CONCAT(`sce1`.`value`, ' ',`sce2`.`value`) AS customer_name"));

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id',
            array(
                'header'=> $this->__('ID'),
                'align' =>'right',
                'width' => '50px',
                'index' => 'id'
            )
        );

        $this->addColumn('customer_name', array(
            'header' => Mage::helper('tagweb_testimonials')->__('Customer Name'),
            'index' => 'customer_name',
            'filter_index' => "CONCAT(`sce1`.`value`, ' ',`sce2`.`value`)"
        ));

        $this->addColumn('customer_mail', array(
            'header' => Mage::helper('tagweb_testimonials')->__('Customer Mail'),
            'index' => 'customer_mail',
            'filter_index' => 'sce3.email'
        ));

        $this->addColumn('comment',
            array(
                'header'=> $this->__('Comment'),
                'index' => 'comment'
            )
        );


        $edit= Mage::helper('adminhtml')->getUrl('testimonials/adminhtml_post/edit') .'id/$id';
        $delete= Mage::helper('adminhtml')->getUrl('testimonials/adminhtml_post/delete') .'id/$id';
        $this->addColumn('actions', array(
            'header'   => $this->helper('catalog')->__('Actions'),
            'width'    => 15,
            'sortable' => false,
            'filter'   => false,
            'type'     => 'action',
            'actions'  => array(
                array(
                    'url'     => $edit,
                    'caption' => $this->helper('catalog')->__('Edit'),
                ),
                array(
                    'url'     => $delete,
                    'caption' => $this->helper('catalog')->__('Delete'),
                ),
            )
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}