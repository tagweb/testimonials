<?php
class Tagweb_Testimonials_Block_Adminhtml_Post_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Init class
     */
    public function __construct()
    {
        $this->_blockGroup = 'tagweb_testimonials';
        $this->_controller = 'adminhtml_post';

        parent::__construct();
    }

    /**
     * Get Header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('tagweb_testimonials')->getId()) {
            return $this->__('Edit Testimonial');
        }
        else {
            return $this->__('New Testimonial');
        }
    }
}