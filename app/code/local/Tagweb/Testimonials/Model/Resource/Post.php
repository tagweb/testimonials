<?php

class Tagweb_Testimonials_Model_Resource_Post extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('tagweb_testimonials/post', 'id');
    }

}