<?php

class Tagweb_Testimonials_Model_Post extends Mage_Core_Model_Abstract{

    protected function _construct()
    {
        $this->_init('tagweb_testimonials/post');
    }

    public function getPosts(){

        $posts = array();

        $testimonials = Mage::getModel('tagweb_testimonials/post')
            ->getCollection()
            ->getData();

        foreach($testimonials as $key => $testimonial){
            $posts[$key]['name'] = $this->_getCustomerName($testimonial['customer_id']);
            $posts[$key]['comment'] = $testimonial['comment'];
        }

        return $posts;
    }

    private function _getCustomerName($customerId){
        return Mage::getModel('customer/customer')->load($customerId)->getName();
    }


}